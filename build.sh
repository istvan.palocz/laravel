#!/bin/bash
docker build --target composer -t tanarurkerem/laravel:composer .
docker build --target installer -t tanarurkerem/laravel:installer .
docker build --target web -t tanarurkerem/laravel:web -t tanarurkerem/laravel .
docker build --target dev -t tanarurkerem/laravel:dev .
docker build --target node -t tanarurkerem/laravel:node .
