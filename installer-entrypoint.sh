#!/bin/bash
  laravel new "$@"
  cp -arT /example /project
  chmod -R a+w /project/storage
  chmod -R a+w /project/bootstrap/cache
